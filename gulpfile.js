const gulp = require('gulp');
const postcss = require('gulp-postcss');
const handlebars = require('gulp-hb');
const rename = require('gulp-rename');
const gutil = require('gulp-util');
const plugins = [
	require('postcss-import')({
		path: ['./src/stylesheets'],
		root: './src/stylesheets'
	}),
	require('postcss-clearfix'),
	require('postcss-short'),
	require('postcss-cssnext')
];

// CSS task.
gulp.task('css', () =>
	gulp.src('src/stylesheets/index.css')
		.pipe(postcss(plugins).on('error', gutil.log))
		.pipe(gulp.dest('./public'))
);

// Templates task.
gulp.task('html', () =>
	gulp.src([
		'src/templates/rubrikaSpec/rubrikaSpec.hbs',
		'src/templates/rubrika/rubrika.hbs',
		'src/templates/hashtag/hashtag.hbs',
		'src/templates/article/article.hbs',
		'src/templates/search/search.hbs',
		'src/templates/post/post.hbs',
		'src/templates/index/index.hbs',
		'src/templates/sitemap.hbs'
	]).pipe(handlebars({
		partials: 'src/templates/**/*.hbs',
		data: require('./data'),
		helpers: {
			ifFirst: function(index, options) {
			  return index === 0 ? options.fn(this) : options.inverse(this);
			},
			eq: (a, b) => a === b,
			gt: (a, b) => a > b,
			gte: (a, b) => a >= b,
			lt: (a, b) => a < b,
			lte: (a, b) => a <= b
		}
	}).on('error', gutil.log))
		.pipe(rename({extname: '.html'}))
		.pipe(gulp.dest('./public'))
);

// Watch task.
gulp.task('watch', () => {
	const browserSync = require('browser-sync').create();
	const watch = require('gulp-watch');

	browserSync.init({open: false, server: {baseDir: './public'}});

	watch('src/stylesheets/**.css', () => gulp.run('css'));
	watch('src/templates/**.hbs', () => gulp.run('html'));
	watch('public/**/*', browserSync.reload);
});
